// Minimum jump power to sortof match Vsh1's Super Duper Jump, which maxed at 3075
// The extra 25 are negligtible, but adds a little extra power, which could possibly be useful.
static const float MinEndangeredJumpPower = 3100.0;

void CreateVsh1SuperJump()
{
	AddDefinitionCreatedListener(BFF_NORMAL_CHARGE_ABILITY, DefinitionCreated);
}

static void DefinitionCreated()
{
	NormalChargeAbilityCreator creator = new NormalChargeAbilityCreator("BFFDA_NCA_Vsh1SuperJump", OnChargeReleased);

	creator.DefineFloat("BasePower", true, 0.0, false);
	creator.DefineFloat("ScaledPower", true, 0.0, false);
	creator.DefineFloat("EndangeredPower", true, 1000.0, false);
	creator.DefineFloat("MinLookUpAngle", true, 0.0, true, 89.0);
	
	creator.SetFloat("BasePower", 750.0);
	creator.SetFloat("ScaledPower", 325.0);
	creator.SetFloat("EndangeredPower", 2000.0);
	creator.SetFloat("MinLookUpAngle", 45.0);

	creator.SetCanSaveFromDanger(true);
	creator.Finalize();
}

static bool OnChargeReleased(BffClient client, float chargePercent, Definition ability)
{
	float eyeAngles[3];
	if (GetClientEyeAngles(client.Index, eyeAngles) && eyeAngles[0] <= -ability.GetFloatEx("MinLookUpAngle"))
	{
		float vel[3];
		client.GetEntPropVector(Prop_Data, "m_vecVelocity", vel);

		bool endangered = client.IsEndangered;

		float basePower = ability.GetFloatEx("BasePower");
		float chargeMultiplier = ability.GetFloatEx("ScaledPower");

		float jumpPower = basePower + (chargePercent * chargeMultiplier);

		// endangered = same condition that triggers SuperDuperJump in VSH
		if (endangered)
		{
			jumpPower += ability.GetFloatEx("EndangeredPower");

			// We'll have to set a minimum jump power, as otherwise the boss might not be able
			// to get out of pits. It'll still have different powere depending on charge,
			// it'll just subtract ((1-charge) * chargeMultplier) from the min endangered jump power instead
			if (jumpPower < MinEndangeredJumpPower)
			{
				float inverseCharge = (1.0 - chargePercent);
				jumpPower = MinEndangeredJumpPower - (inverseCharge * chargeMultiplier);
			}
		}
		
		client.SetEntProp(Prop_Send, "m_bJumping", 1);

		// Olden Super Jump had it's charge from 0-25, chargePercent goes from 0-1
		// So multiply with 25 to get matching effect
		vel[0] *= (1 + Sine((chargePercent * 25.0) * FLOAT_PI / 50.0));
		vel[1] *= (1 + Sine((chargePercent * 25.0) * FLOAT_PI / 50.0));
		vel[2] = jumpPower;

		TeleportEntity(client.Index, NULL_VECTOR, NULL_VECTOR, vel);

		client.PlaySound("JumpSounds", true, false);
		return true;
	}
	return false;
}