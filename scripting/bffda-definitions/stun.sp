static ArrayList g_hDelayTimers[MAXPLAYERS+1];

void CreateStun()
{
	AddDefinitionCreatedListener(BFF_SPECIAL_ABILITY, DefinitionCreated);
	AddRoundStateListener(RoundStateChange);
}

static void DefinitionCreated()
{
	SpecialAbilityCreator creator = new SpecialAbilityCreator("BFFDA_SA_Stun", OnUse);

	creator.DefineFloat("Range", true, 50.0, false);
	creator.DefineFloat("DropOffRange", true, -1.0, false);
	creator.DefineFloat("StunTime", true, 0.0, false);
	// Defaults to StunTime if it's < 0.0
	creator.DefineFloat("FarStunTime", true, -1.0, false);
	creator.DefineFloat("Slowdown", true, 0.0, true, 1.0);
	creator.DefineFloat("Delay", true, 0.0, false);
	
	creator.SetFloat("Range", 800.0);
	creator.SetFloat("DropOffRange", 250.0);
	creator.SetFloat("StunTime", 5.0);
	creator.SetFloat("FarStunTime", -1.0);
	creator.SetFloat("Slowdown", 0.0);
	creator.SetFloat("Delay", 0.0);
	creator.SetInt("StunFlags", TF_STUNFLAGS_GHOSTSCARE|TF_STUNFLAG_NOSOUNDOREFFECT);
	
	creator.SetBool("SpawnStunParticle", true);
	creator.SetString("StunParticle", "yikes_fx");
	
	creator.Finalize();
}

static void OnUse(BffClient client, float chargePercent, Definition ability)
{
	float delay = ability.GetFloatEx("Delay");

	if (delay == 0.0)
	{
		PerformStun(client, chargePercent, ability);
	}
	else
	{
		DataPack pack;
		Handle timer = CreateDataTimer(delay, DelayStunTimer, pack, TIMER_FLAG_NO_MAPCHANGE);
		AddTimer(client.Index, timer);
		pack.WriteCell(client);
		pack.WriteCell(client.UserId);
		pack.WriteFloat(chargePercent);
		pack.WriteCell(ability);
	}
}

static Action DelayStunTimer(Handle timer, DataPack pack)
{
	int clientIndex = pack.ReadCell();
	RemoveTimer(clientIndex, timer);

	int userId = pack.ReadCell();
	BffClient client = BffClient(userId, true);
	if (client.IsValid && client.IsBoss)
	{
		PerformStun(client, pack.ReadFloat(), pack.ReadCell());
	}
	return Plugin_Stop;
}

static void PerformStun(BffClient client, float chargePercent, Definition ability)
{
	float range = ability.GetFloatEx("Range");
	float dropOffRange = ability.GetFloatEx("DropOffRange");
	float stunTime = ability.GetFloatEx("StunTime");
	float farStunTime = ability.GetFloatEx("FarStunTime");
	float slowdown = ability.GetFloatEx("Slowdown");
	int stunFlags = ability.GetIntEx("StunFlags");

	char particle[32];
	ability.GetString("StunParticle", particle, sizeof(particle));
	bool spawnParticle = ability.GetBoolEx("SpawnStunParticle");

	farStunTime = (farStunTime < 0.0 ? stunTime : farStunTime) * chargePercent;
	stunTime = stunTime * chargePercent;
	
	dropOffRange = (dropOffRange < 0.0 ? range : dropOffRange) * chargePercent;
	range = range * chargePercent;

	// If there's sound and effect, and it's ghost effect, make attacker the world
	// otherwise it's playing normal stun sounds, not yikes
	int attacker = client.Index;
	if ((stunFlags & TF_STUNFLAG_NOSOUNDOREFFECT) != TF_STUNFLAG_NOSOUNDOREFFECT &&
		(stunFlags & TF_STUNFLAG_GHOSTEFFECT) == TF_STUNFLAG_GHOSTEFFECT)
	{
		attacker = 0;
	}

	float origin[3];
	float targetPos[3];
	float particleOffset[3];
	client.GetEntPropVector(Prop_Send, "m_vecOrigin", origin);
	for (int i = 1; i <= MaxClients; i++)
	{
		BffClient target = BffClient(i);
		if (target.IsValid && target.IsPlaying && target.IsAlive && target.Team != client.Team)
		{
			target.GetEntPropVector(Prop_Send, "m_vecOrigin", targetPos);
			float distance = GetVectorDistance(origin, targetPos);
			if (distance <= range)
			{
				float appliedStunTime = GetStunTime(stunTime, farStunTime, distance, range, dropOffRange);
				target.Stun(appliedStunTime, slowdown, stunFlags, attacker);
				if (spawnParticle)
				{
					GetClientEyePosition(target.Index, particleOffset);
					particleOffset[0] = 0.0;
					particleOffset[1] = 0.0;
					particleOffset[2] = particleOffset[2] - targetPos[2];
					BFF_SpawnParticleNear(target.Index, particle, appliedStunTime, particleOffset, true);
				}
			}
		}
	}
}

static float GetStunTime(float stunTime, float farStunTime, float distance, float range, float dropOffRange)
{
	if (distance <= dropOffRange)
	{
		return stunTime;
	}
	distance -= dropOffRange;
	range -= dropOffRange;
	return farStunTime + ((stunTime - farStunTime) * ((range - distance) / range));
}

// As long as round state changes, we just want to kill off the delay timers
static void RoundStateChange()
{
	for (int i = 1; i <= MaxClients; i++)
	{
		if (g_hDelayTimers[i] != null)
		{
			int length = g_hDelayTimers[i].Length;
			for (int j = 0; j < length; j++)
			{
				Handle timer = g_hDelayTimers[i].Get(j);
				delete timer;
			}
			delete g_hDelayTimers[i];
		}
	}
}

static void AddTimer(int client, Handle timer)
{
	if (g_hDelayTimers[client] == null)
	{
		g_hDelayTimers[client] = new ArrayList();
	}
	g_hDelayTimers[client].Push(timer);
}

static void RemoveTimer(int client, Handle timer)
{
	int index = g_hDelayTimers[client].FindValue(timer);
	if (index != -1)
	{
		g_hDelayTimers[client].Erase(index);
		if (g_hDelayTimers[client].Length == 0)
		{
			delete g_hDelayTimers[client];
		}
	}
}