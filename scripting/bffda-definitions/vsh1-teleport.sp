void CreateVsh1Teleport()
{
	AddDefinitionCreatedListener(BFF_NORMAL_CHARGE_ABILITY, DefinitionCreated);
}

static void DefinitionCreated()
{
	NormalChargeAbilityCreator creator = new NormalChargeAbilityCreator("BFFDA_NCA_Vsh1Teleport", OnChargeReleased);

	creator.DefineFloat("SelfStunTime", true, 0.0, false);
	creator.DefineFloat("EndangeredSelfStunTime", true, 0.0, false);

	creator.SetFloat("SelfStunTime", 4.0);
	creator.SetFloat("EndangeredSelfStunTime", 2.0);
	
	// Allow it to be above 100%, to allow attack
	creator.DefineFloat("PreventAttackChargePercent", true, 0.0, true, 1.1);
	creator.SetFloat("PreventAttackChargePercent", 0.5);

	creator.SetBool("SpawnParticle", true);
	creator.SetFloat("ParticleTime", 3.0);
	creator.SetString("Particle", "ghost_appearation");
	creator.SetBool("AttachDestinationParticle", true);

	// These can be used, but aren't set by default
	// They default to their non-destination variant
	// creator.SetBool("SpawnDestinationParticle", true);
	// creator.SetFloat("DestinationParticleTime", 3.0);
	// creator.SetString("DestinationParticle", "ghost_appearation");

	creator.SetCanSaveFromDanger(true);
	creator.SetOnPlayerRunCmd(OnChargingPlayerRunCmd);

	creator.Finalize();
}

static Action OnChargingPlayerRunCmd(BffClient client, float chargePercent, int &buttons, int &impulse, float vel[3], float ang[3], int &weapon, Definition ability)
{
	#pragma unused ang, vel, client
	if ((buttons & IN_ATTACK) == IN_ATTACK && chargePercent >= ability.GetFloatEx("PreventAttackChargePercent"))
	{
		buttons &= ~IN_ATTACK;
	}
	return Plugin_Continue;
}

static bool OnChargeReleased(BffClient client, float chargePercent, Definition ability)
{
	// can only be used at max charge
	if (chargePercent != 1.0)
	{
		return false;
	}

	int potentialTargetsCount = 0;
	BffClient[] potentialTargetsArray = new BffClient[MaxClients];
	
	TFTeam clientTeam = client.Team;
	for (int i = 1; i <= MaxClients; i++)
	{
		BffClient target = BffClient(i);
		if (target.IsValid && target.IsAlive && target.Team != clientTeam)
		{
			potentialTargetsArray[potentialTargetsCount++] = target;
		}
	}
	
	BffClient target;
	if (potentialTargetsCount > 0)
	{
		target = potentialTargetsArray[GetRandomInt(0, potentialTargetsCount - 1)];
	}
	else
	{
		// no targets :(
		return false;
	}


	float selfStunTime = client.IsEndangered ? ability.GetFloatEx("EndangeredSelfStunTime") : ability.GetFloatEx("SelfStunTime");

	// the destination options are optional, and defaults to origin stuff, 
	// which doesn't have the origin prefix for simplicity in most use cases
	char originParticle[32];
	char destParticle[32];

	bool spawnOriginParticle = ability.GetBoolEx("SpawnParticle");
	float originParticleTime = ability.GetFloatEx("ParticleTime");
	ability.GetString("Particle", originParticle, sizeof(originParticle));

	bool spawnDestParticle = ability.GetBoolEx("SpawnDestinationParticle", spawnOriginParticle);
	float destParticleTime = ability.GetFloatEx("DestinationParticleTime", originParticleTime);
	ability.GetStringEx("DestinationParticle", destParticle, sizeof(destParticle), originParticle);
	bool attachDestParticle = ability.GetBoolEx("AttachDestinationParticle");

	float originPos[3];
	client.GetEntPropVector(Prop_Send, "m_vecOrigin", originPos);

	if (TeleportClientToTarget(client, target))
	{
		// Chdata's HHH teleport rework
		TFClassType targetClass = target.Class;
		if (targetClass != TFClass_Scout && targetClass != TFClass_Soldier)
		{
			DataPack pack;

			CreateDataTimer(selfStunTime, TimerReverseCollisionGroup, pack, TIMER_FLAG_NO_MAPCHANGE);
			pack.WriteCell(client.UserId);
			pack.WriteCell(client.GetEntProp(Prop_Send, "m_CollisionGroup"));

			client.SetEntProp(Prop_Send, "m_CollisionGroup", 2); //Makes HHH clipping go away for player and some projectiles
		}

		client.SetEntPropFloat(Prop_Send, "m_flNextAttack", GetGameTime() + selfStunTime);

		// The following is nothing built-in to BFF, and shouldn't be in my mind
		// Should probably make a globalforward instead
		//SetEntProp(Hale, Prop_Send, "m_bGlowEnabled", 0);
		//GlowTimer = 0.0;

		if (spawnOriginParticle)
		{
			BFF_SpawnParticleAt(originPos, originParticle, originParticleTime); // One is parented and one is not
		}

		client.Stun(selfStunTime, 0.0, TF_STUNFLAGS_GHOSTSCARE|TF_STUNFLAG_NOSOUNDOREFFECT, target.Index);

		if (spawnDestParticle)
		{
			BFF_SpawnParticleNear(client.Index, destParticle, destParticleTime, _, attachDestParticle); // So the teleport smoke appears at both destinations
		}
		
		client.PlaySound("TeleportSounds", true, false);
		return true;
	}
	return false;
}

static Action TimerReverseCollisionGroup(Handle timer, DataPack data)
{
	#pragma unused timer
	data.Reset();
	BffClient client = BffClient(data.ReadCell(), true);
	if (client.IsValid)
	{
		if (client.GetEntProp(Prop_Send, "m_CollisionGroup") == 2)
		{
			client.SetEntProp(Prop_Send, "m_CollisionGroup", data.ReadCell());
		}
	}
	return Plugin_Stop;
}

static bool TeleportClientToTarget(BffClient client, BffClient target)
{
	float targetPos[3];
	target.GetEntPropVector(Prop_Send, "m_vecOrigin", targetPos);

	DataPack pack = new DataPack();
	pack.WriteCell(client);
	pack.WriteCell(target);
	bool result = client.TeleportTo(targetPos, _, TeleportFilter, pack);
	delete pack;

	return result;
}

static bool TeleportFilter(int entity, int contentsMask, any data)
{
	#pragma unused contentsMask
	DataPack pack = view_as<DataPack>(data);
	pack.Reset();
	BffClient client = BffClient(pack.ReadCell());
	BffClient target = BffClient(pack.ReadCell());
	if (client.Index == entity || target.Index == entity)
	{
		return false;
	}
	if (entity > 0 && entity <= MaxClients && TF2_GetClientTeam(entity) == client.Team)
	{
		return false;
	}
	return true;
}