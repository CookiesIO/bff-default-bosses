void CreateWeighdown()
{
	AddDefinitionCreatedListener(BFF_CONTINUOUS_CHARGE_ABILITY, DefinitionCreated);
}

static void DefinitionCreated()
{
	ContinuousChargeAbilityCreator creator = new ContinuousChargeAbilityCreator("BFFDA_CCA_Weighdown", OnChargeStart, OnChargeReleased);

	creator.DefineFloat("GravityMultiplier", true, 1.0, false);
	creator.DefineFloat("DownwardImpulse", true, 0.0, false);
	creator.DefineFloat("MinLookDownAngle", true, 0.0, true, 89.0);
	
	creator.SetFloat("GravityMultiplier", 6.0);
	creator.SetFloat("DownwardImpulse", 1000.0);
	creator.SetFloat("MinLookDownAngle", 45.0);
	creator.SetBool("FollowLookAngle", true);

	creator.SetIsAerialEscape(true);
	creator.Finalize();
}

static bool OnChargeStart(BffClient client, float cooldownPercent, float cooldownDifference, Definition ability)
{
	float eyeAngles[3];
	if (
		(cooldownPercent == 1.0 || cooldownDifference == 0.1) &&
		GetClientEyeAngles(client.Index, eyeAngles) && eyeAngles[0] >= ability.GetFloatEx("MinLookDownAngle")
	)
	{
		float vel[3];

		if (ability.GetBoolEx("FollowLookAngle"))
		{
			GetAngleVectors(eyeAngles, vel, NULL_VECTOR, NULL_VECTOR);
			NormalizeVector(vel, vel);
			ScaleVector(vel, ability.GetFloatEx("DownwardImpulse"));
		}
		else
		{
			client.GetEntPropVector(Prop_Data, "m_vecVelocity", vel);
			vel[2] -= ability.GetFloatEx("DownwardImpulse");
		}

		SetEntityGravity(client.Index, ability.GetFloatEx("Gravity"));
		TeleportEntity(client.Index, NULL_VECTOR, NULL_VECTOR, vel);
		return true;
	}
	return false;
}

static void OnChargeReleased(BffClient client, float cooldownPercent, float cooldownDifference, Definition ability)
{
	#pragma unused cooldownPercent, cooldownDifference, ability
	SetEntityGravity(client.Index, 1.0);
}