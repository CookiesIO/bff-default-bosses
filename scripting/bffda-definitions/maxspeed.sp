static float g_flPreviousMaxspeed[MAXPLAYERS+1];
static float g_flDesiredStartMaxspeed[MAXPLAYERS+1];
static float g_flDesiredTargetMaxspeed[MAXPLAYERS+1];
static float g_flDesiredTargetReachedAt[MAXPLAYERS+1];

void CreateMaxspeed()
{
	AddDefinitionCreatedListener(BFF_CONTINUOUS_CHARGE_ABILITY, DefinitionCreated);
}

static void DefinitionCreated()
{
	ContinuousChargeAbilityCreator creator = new ContinuousChargeAbilityCreator("BFFDA_CCA_Maxspeed", OnChargeStart, OnChargeReleased);

	creator.DefineFloat("Maxspeed", true, 1.0, true, 520.0);
	creator.DefineFloat("ReachTargetSpeedAt", true, 0.0, true, 1.0);
	creator.SetFloat("Maxspeed", 520.0);
	creator.SetFloat("ReachTargetSpeedAt", 1.0);
	// Set TargetMaxspeed if you want max speed to change over time to that
	//creator.SetFloat("TargetMaxspeed", 520.0)

	creator.SetOnPlayerRunCmd(OnChargeRunCmd);
	creator.Finalize();
}

static bool OnChargeStart(BffClient client, float cooldownPercent, float cooldownDifference, Definition ability)
{
	#pragma unused cooldownPercent, cooldownDifference

	float desiredStartMaxspeed = ability.GetFloatEx("Maxspeed");
	float desiredTargetMaxspeed = ability.GetFloatEx("TargetMaxspeed", desiredStartMaxspeed);
	float desiredTargetReachedAt = ability.GetFloatEx("ReachTargetSpeedAt");

	g_flDesiredStartMaxspeed[client.Index] = desiredStartMaxspeed;
	g_flDesiredTargetMaxspeed[client.Index] = desiredTargetMaxspeed;
	g_flDesiredTargetReachedAt[client.Index] = desiredTargetReachedAt;

	g_flPreviousMaxspeed[client.Index] = GetEntPropFloat(client.Index, Prop_Send, "m_flMaxspeed");
	client.SetEntPropFloat(Prop_Send, "m_flMaxspeed", g_flDesiredStartMaxspeed[client.Index]);
	return true;
}

static void OnChargeReleased(BffClient client, float chargePercent)
{
	#pragma unused chargePercent
	client.SetEntPropFloat(Prop_Send, "m_flMaxspeed", g_flPreviousMaxspeed[client.Index]);
}

static Action OnChargeRunCmd(BffClient client, float chargePercent, int &buttons, int &impulse, float vel[3], float ang[3], int &weapon, Definition ability)
{
	#pragma unused ang, vel, chargePercent, ability
	float desiredStartMaxspeed = g_flDesiredStartMaxspeed[client.Index];
	float desiredTargetMaxspeed = g_flDesiredTargetMaxspeed[client.Index];
	float desiredTargetReachedAt = g_flDesiredTargetReachedAt[client.Index];

	float maxspeed = desiredTargetMaxspeed;
	if (desiredStartMaxspeed != desiredTargetMaxspeed && desiredTargetReachedAt < chargePercent)
	{
		float mult = chargePercent / desiredTargetReachedAt;
		if (mult > 1.0)
		{
			mult = 1.0;
		}
		maxspeed = desiredStartMaxspeed + ((desiredTargetMaxspeed - desiredStartMaxspeed) * (chargePercent / desiredTargetReachedAt));
	}

	client.SetEntPropFloat(Prop_Send, "m_flMaxspeed", maxspeed);
	return Plugin_Continue;
}