#pragma semicolon 1

#include <tf2items>

#pragma newdecls required

#include <sourcemod>
#include <tf2>
#include <tf2_stocks>
#include <sdkhooks>
#include <definitions>
#include <bossfightfortress-base>

#define PLUGIN_VERSION "1.0"

public Plugin myinfo = 
{
	name = "Boss Fight Fortress - Horseless Headless Horsemann Jr.",
	author = "Cookies.io",
	description = "Horseless Headless Horsemann Jr. akin to VSH1 for Boss Fight Fortress.",
	version = PLUGIN_VERSION,
	url = "http://forums.alliedmods.net"
};

public void OnPluginStart()
{
	CreateConVar("bossfightfortress_hhh_version", PLUGIN_VERSION, "Boss Fight Fortress - Horseless Headless Horsemann Jr. Version", FCVAR_NOTIFY|FCVAR_DONTRECORD|FCVAR_CHEAT);
}

public void OnAllPluginsLoaded()
{
	AddDefinitionCreatedListener(BFF_BOSS_GENERIC, BffGenericBossAvailable);
	AddDefinitionCreatedListener("BFFDA_SA_Stun", BffdaStunAbilityAvailable);
}

public void OnPluginEnd()
{
	DestroyMyDefinitions();
}

public void BffGenericBossAvailable(Definition definition)
{
	BffBossCreator creator = new BffBossCreator("Horseless Headless Horsemann Jr.", TFClass_DemoMan, GetMaxHealth, EquipBoss);

	// Abilities
	creator.SetAbility(AbilitySlot_Primary, 0, "BFFDA_NCA_Vsh1Teleport", "Teleport", 2.0, 44.0);
	creator.SetAbility(AbilitySlot_Secondary, 0, "BFFDA_CCA_Weighdown", "Weighdown", 5.0, 2.0);

	creator.SetSpecialAbility(0, "Scare", SpecialChargeMode_Assisting, 3500, 0.0, false, OnUseScareAbility);
	creator.AddSpecialAbility(0, "HHH_Stun");

	// File setup
	creator.SetCustomModel("models/player/saxton_hale/hhh_jr_mk3.mdl");

	// Spawn sound
	creator.PushStringToArray("SoundsOnSpawn", "ui/halloween_boss_summoned_fx.wav");

	// Catch phrases
	creator.PushStringToArray("SoundsCatchPhrase", "vo/halloween_boss/knight_laugh01.mp3");
	creator.PushStringToArray("SoundsCatchPhrase", "vo/halloween_boss/knight_laugh02.mp3");
	creator.PushStringToArray("SoundsCatchPhrase", "vo/halloween_boss/knight_laugh03.mp3");
	creator.PushStringToArray("SoundsCatchPhrase", "vo/halloween_boss/knight_laugh04.mp3");

	// Kill sounds
	creator.PushStringToArray("SoundsOnKillAny", "vo/halloween_boss/knight_attack01.mp3");
	creator.PushStringToArray("SoundsOnKillAny", "vo/halloween_boss/knight_attack02.mp3");
	creator.PushStringToArray("SoundsOnKillAny", "vo/halloween_boss/knight_attack03.mp3");
	creator.PushStringToArray("SoundsOnKillAny", "vo/halloween_boss/knight_attack04.mp3");

	creator.PushStringToArray("SoundsOnKillingSpree", "vo/halloween_boss/knight_laugh01.mp3");
	creator.PushStringToArray("SoundsOnKillingSpree", "vo/halloween_boss/knight_laugh02.mp3");
	creator.PushStringToArray("SoundsOnKillingSpree", "vo/halloween_boss/knight_laugh03.mp3");
	creator.PushStringToArray("SoundsOnKillingSpree", "vo/halloween_boss/knight_laugh04.mp3");
	
	// Scare sounds
	creator.DefineStringArray("SoundsOnHHHScare", PLATFORM_MAX_PATH);
	creator.PushStringToArray("SoundGroups", "SoundsOnHHHScare");
	creator.PushStringToArray("SoundsOnHHHScare", "vo/halloween_boss/knight_attack01.mp3");
	creator.PushStringToArray("SoundsOnHHHScare", "vo/halloween_boss/knight_alert.mp3");

	creator.SetBool("MuteVoice", true);
	creator.Finalize();
}

public void BffdaStunAbilityAvailable(Definition definition)
{
	DefinitionCreator creator = new DefinitionCreator("HHH_Stun", definition);

	creator.SetInt("StunFlags", TF_STUNFLAGS_GHOSTSCARE);
	creator.SetBool("SpawnStunParticle", false);

	creator.Finalize();
}

public int GetMaxHealth(float multiplier)
{
	// Chdata's slightly reworked Hale HP calculation
	return 2048 + RoundFloat(Pow(((760.8 + multiplier) * (multiplier - 1)), 1.0341));
}

public void EquipBoss(BffClient client)
{
	client.RemoveAllWeaponsAndWearables();
	int weapon = client.GiveWeapon("tf_weapon_sword", 266, "68 ; 2.0 ; 2 ; 3.1 ; 259 ; 1.0 ; 252 ; 0.6 ; 551 ; 1", TFQuality_Unusual, 100, true, false);
}

public bool OnUseScareAbility(BffClient client, float chargePercent)
{
	if (chargePercent == 1.0)
	{
		client.PlaySound("SoundsOnHHHScare", true, false);
		return true;
	}
	return false;
}
