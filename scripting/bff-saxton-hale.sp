#pragma semicolon 1

#include <tf2items>

#pragma newdecls required

#include <sourcemod>
#include <tf2>
#include <tf2_stocks>
#include <sdkhooks>
#include <definitions>
#include <bossfightfortress-base>

#define PLUGIN_VERSION "1.0"

public Plugin myinfo = 
{
	name = "Boss Fight Fortress - Saxton Hale",
	author = "Cookies.io",
	description = "Saxton Hale akin to VSH1 for Boss Fight Fortress.",
	version = PLUGIN_VERSION,
	url = "http://forums.alliedmods.net"
};

public void OnPluginStart()
{
	CreateConVar("bossfightfortress_saxton_hale_version", PLUGIN_VERSION, "Boss Fight Fortress - Saxton Hale Version", FCVAR_NOTIFY|FCVAR_DONTRECORD|FCVAR_CHEAT);
}

public void OnAllPluginsLoaded()
{
	AddDefinitionCreatedListener(BFF_BOSS_GENERIC, BffGenericBossAvailable);
}

public void OnPluginEnd()
{
	DestroyMyDefinitions();
}

public void BffGenericBossAvailable(Definition definition)
{
	BffBossCreator creator = new BffBossCreator("Saxton Hale", TFClass_Soldier, GetMaxHealth, EquipBoss);
	creator.SetSoundHook(SoundHook);

	// Abilities
	creator.SetAbility(AbilitySlot_Primary, 0, "BFFDA_NCA_Vsh1SuperJump", "Super Jump", 1.0, 24.0);
	creator.SetAbility(AbilitySlot_Secondary, 0, "BFFDA_CCA_Weighdown", "Weighdown", 5.0, 2.0);

	creator.SetSpecialAbility(0, "Frighten", SpecialChargeMode_Assisting, 3500, 0.0, false, OnUseFrightenAbility);
	creator.AddSpecialAbility(0, "BFFDA_SA_Stun");

	// File setup
	creator.SetCustomModel("models/player/saxton_test4/saxton_hale_test4.mdl");

	// The hale model materials we need to get downloaded
	creator.PushStringToArray("Materials", "materials/models/player/saxton_test4/eyeball_l");
	creator.PushStringToArray("Materials", "materials/models/player/saxton_test4/eyeball_r");
	creator.PushStringToArray("Materials", "materials/models/player/saxton_test4/halebody");
	creator.PushStringToArray("Materials", "materials/models/player/saxton_test4/halebodyexponent");
	creator.PushStringToArray("Materials", "materials/models/player/saxton_test4/halehead");
	creator.PushStringToArray("Materials", "materials/models/player/saxton_test4/haleheadexponent");
	creator.PushStringToArray("Materials", "materials/models/player/saxton_test4/halenormal");
	creator.PushStringToArray("Materials", "materials/models/player/saxton_test4/halephongmask");

	// Spawn sounds
	creator.PushStringToArray("SoundsOnSpawn", "saxton_hale/saxton_hale_responce_start1.wav");
	creator.PushStringToArray("SoundsOnSpawn", "saxton_hale/saxton_hale_responce_start2.wav");
	creator.PushStringToArray("SoundsOnSpawn", "saxton_hale/saxton_hale_responce_start3.wav");
	creator.PushStringToArray("SoundsOnSpawn", "saxton_hale/saxton_hale_responce_start4.wav");
	creator.PushStringToArray("SoundsOnSpawn", "saxton_hale/saxton_hale_responce_start5.wav");
	creator.PushStringToArray("SoundsOnSpawn", "saxton_hale/saxton_hale_132_start_1.wav");
	creator.PushStringToArray("SoundsOnSpawn", "saxton_hale/saxton_hale_132_start_2.wav");
	creator.PushStringToArray("SoundsOnSpawn", "saxton_hale/saxton_hale_132_start_3.wav");
	creator.PushStringToArray("SoundsOnSpawn", "saxton_hale/saxton_hale_132_start_4.wav");
	creator.PushStringToArray("SoundsOnSpawn", "saxton_hale/saxton_hale_132_start_5.wav");

	// Win sounds
	creator.PushStringToArray("SoundsOnWin", "saxton_hale/saxton_hale_responce_win1.wav");
	creator.PushStringToArray("SoundsOnWin", "saxton_hale/saxton_hale_responce_win2.wav");

	// Lose sounds
	creator.PushStringToArray("SoundsOnLoss", "saxton_hale/saxton_hale_responce_fail1.wav");
	creator.PushStringToArray("SoundsOnLoss", "saxton_hale/saxton_hale_responce_fail2.wav");
	creator.PushStringToArray("SoundsOnLoss", "saxton_hale/saxton_hale_responce_fail3.wav");

	// Kill sounds
	creator.PushStringToArray("SoundsOnKillScout", "saxton_hale/saxton_hale_132_kill_scout.wav");
	creator.PushStringToArray("SoundsOnKillPyro", "saxton_hale/saxton_hale_132_kill_w_and_m1.wav");
	creator.PushStringToArray("SoundsOnKillDemoman", "saxton_hale/saxton_hale_132_kill_demo.wav");
	creator.PushStringToArray("SoundsOnKillHeavy", "saxton_hale/saxton_hale_132_kill_heavy.wav");
	creator.PushStringToArray("SoundsOnKillMedic", "saxton_hale/saxton_hale_responce_kill_medic.wav");
	creator.PushStringToArray("SoundsOnKillSniper", "saxton_hale/saxton_hale_responce_kill_sniper1.wav");
	creator.PushStringToArray("SoundsOnKillSniper", "saxton_hale/saxton_hale_responce_kill_sniper2.wav");
	creator.PushStringToArray("SoundsOnKillSpy", "saxton_hale/saxton_hale_responce_kill_spy1.wav");
	creator.PushStringToArray("SoundsOnKillSpy", "saxton_hale/saxton_hale_responce_kill_spy2.wav");
	creator.PushStringToArray("SoundsOnKillSpy", "saxton_hale/saxton_hale_132_kill_spie.wav");
	creator.PushStringToArray("SoundsOnKillEngineer", "saxton_hale/saxton_hale_responce_kill_eggineer1.wav");
	creator.PushStringToArray("SoundsOnKillEngineer", "saxton_hale/saxton_hale_responce_kill_eggineer2.wav");
	creator.PushStringToArray("SoundsOnKillEngineer", "saxton_hale/saxton_hale_132_kill_engie_1.wav");
	creator.PushStringToArray("SoundsOnKillEngineer", "saxton_hale/saxton_hale_132_kill_engie_2.wav");

	creator.PushStringToArray("SoundsOnKillingSpree", "saxton_hale/saxton_hale_responce_spree1.wav");
	creator.PushStringToArray("SoundsOnKillingSpree", "saxton_hale/saxton_hale_responce_spree2.wav");
	creator.PushStringToArray("SoundsOnKillingSpree", "saxton_hale/saxton_hale_responce_spree3.wav");
	creator.PushStringToArray("SoundsOnKillingSpree", "saxton_hale/saxton_hale_responce_spree4.wav");
	creator.PushStringToArray("SoundsOnKillingSpree", "saxton_hale/saxton_hale_responce_spree5.wav");
	creator.PushStringToArray("SoundsOnKillingSpree", "saxton_hale/saxton_hale_132_kspree_1.wav");
	creator.PushStringToArray("SoundsOnKillingSpree", "saxton_hale/saxton_hale_132_kspree_2.wav");
	creator.PushStringToArray("SoundsOnKillingSpree", "saxton_hale/saxton_hale_responce_3.wav");

	creator.PushStringToArray("SoundsOnDestroyBuilding", "saxton_hale/saxton_hale_132_kill_toy.wav");

	creator.PushStringToArray("SoundsOnBackstabbed", "saxton_hale/saxton_hale_132_stub_1.wav");
	creator.PushStringToArray("SoundsOnBackstabbed", "saxton_hale/saxton_hale_132_stub_2.wav");
	creator.PushStringToArray("SoundsOnBackstabbed", "saxton_hale/saxton_hale_132_stub_3.wav");
	creator.PushStringToArray("SoundsOnBackstabbed", "saxton_hale/saxton_hale_132_stub_4.wav");

	// Sounds when Hale is the last alive, gotta be defined first since this is a BvsA only sound group, it's not provided by default
	creator.AddSoundGroup("SoundsOnLastAlive");
	creator.PushStringToArray("SoundsOnLastAlive", "saxton_hale/saxton_hale_responce_2.wav");
	creator.PushStringToArray("SoundsOnLastAlive", "vo/announcer_am_lastmanalive01.mp3");
	creator.PushStringToArray("SoundsOnLastAlive", "vo/announcer_am_lastmanalive02.mp3");
	creator.PushStringToArray("SoundsOnLastAlive", "vo/announcer_am_lastmanalive03.mp3");
	creator.PushStringToArray("SoundsOnLastAlive", "vo/announcer_am_lastmanalive04.mp3");

	creator.PushStringToArray("SoundsOnLastAlive", "saxton_hale/saxton_hale_132_last.wav");
	creator.PushStringToArray("SoundsOnLastAlive", "saxton_hale/saxton_hale_responce_lastman1.wav");
	creator.PushStringToArray("SoundsOnLastAlive", "saxton_hale/saxton_hale_responce_lastman2.wav");
	creator.PushStringToArray("SoundsOnLastAlive", "saxton_hale/saxton_hale_responce_lastman3.wav");
	creator.PushStringToArray("SoundsOnLastAlive", "saxton_hale/saxton_hale_responce_lastman4.wav");
	creator.PushStringToArray("SoundsOnLastAlive", "saxton_hale/saxton_hale_responce_lastman5.wav");
	
	// Scare sounds
	creator.AddSoundGroup("SoundsOnFrighten");
	creator.PushStringToArray("SoundsOnFrighten", "saxton_hale/saxton_hale_responce_rage1.wav");
	creator.PushStringToArray("SoundsOnFrighten", "saxton_hale/saxton_hale_responce_rage2.wav");
	creator.PushStringToArray("SoundsOnFrighten", "saxton_hale/saxton_hale_responce_rage3.wav");
	creator.PushStringToArray("SoundsOnFrighten", "saxton_hale/saxton_hale_responce_rage4.wav");

	// Jump sounds, used by BFFDA_NCA_Vsh1SuperJump
	creator.AddSoundGroup("JumpSounds");
	creator.PushStringToArray("JumpSounds", "saxton_hale/saxton_hale_responce_jump1.wav");
	creator.PushStringToArray("JumpSounds", "saxton_hale/saxton_hale_responce_jump2.wav");
	creator.PushStringToArray("JumpSounds", "saxton_hale/saxton_hale_132_jump_1.wav");
	creator.PushStringToArray("JumpSounds", "saxton_hale/saxton_hale_132_jump_2.wav");

	creator.SetBool("MuteVoice", true);
	creator.Finalize();
}

public int GetMaxHealth(float multiplier)
{
	// Chdata's slightly reworked Hale HP calculation
	return 2048 + RoundFloat(Pow(((760.8 + multiplier) * (multiplier - 1)), 1.0341));
}

public void EquipBoss(BffClient client)
{
	client.RemoveAllWeaponsAndWearables();
	
	char attributes[64];
	Format(attributes, sizeof(attributes), "68;2.0 ; 2;3.1 ; 259;1.0 ; 252;0.6 ; 214;%d", GetRandomInt(9999, 99999));
	client.GiveWeapon("tf_weapon_shovel", 5, attributes, TFQuality_Strange, 100, true, false);
}

public bool OnUseFrightenAbility(BffClient client, float chargePercent)
{
	if (chargePercent == 1.0)
	{
		client.PlaySound("SoundsOnFrighten", true, false);
		return true;
	}
	return false;
}

public Action SoundHook(BffClient client, int clients[MAXPLAYERS], int &numClients, char sample[PLATFORM_MAX_PATH], int &entity, int &channel, float &volume, int &level, int &pitch, int &flags)
{
	if (StrContains(sample, "shovel_swing") != -1)
	{
		if (StrContains(sample, "crit") != -1)
		{
			Format(sample, sizeof(sample), "weapons/fist_swing_crit.wav");
		}
		else
		{
			Format(sample, sizeof(sample), "weapons/bat_draw_swoosh%d.wav", GetRandomInt(1, 2));
		}
		return Plugin_Changed;
	}
	else if (StrContains(sample, "cbar_hit") != -1)
	{
		Format(sample, sizeof(sample), "weapons/fist_hit_world%d.wav", GetRandomInt(1, 2));
		return Plugin_Changed;
	}
	else if (StrContains(sample, "axe_hit_flesh") != -1)
	{
		Format(sample, sizeof(sample), "weapons/cbar_hitbod%d.wav", GetRandomInt(1, 3));
		return Plugin_Changed;
	}
	return Plugin_Continue;
}
