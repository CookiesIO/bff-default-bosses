#pragma semicolon 1
#pragma newdecls required

#include <sourcemod>
#include <definitions>
#include <bossfightfortress-base>
#include <tf2_stocks>

#include "bffda-definitions/vsh1-super-jump.sp"
#include "bffda-definitions/vsh1-teleport.sp"
#include "bffda-definitions/weighdown.sp"
#include "bffda-definitions/maxspeed.sp"
#include "bffda-definitions/stun.sp"

#define PLUGIN_VERSION "1.0"

Handle roundStateChangeForward;

public Plugin myinfo = 
{
	name = "BFF - Default Abilities",
	author = "Cookies.io",
	description = "Default abilities for Boss Fight Fortress.",
	version = PLUGIN_VERSION,
	url = "http://forums.alliedmods.net"
};

public void OnPluginStart()
{
	CreateConVar("bossfightfortress_default_abilities_version", PLUGIN_VERSION, "Boss Fight Fortress - Default Abilities Version", FCVAR_NOTIFY|FCVAR_DONTRECORD|FCVAR_CHEAT);
	roundStateChangeForward = CreateForward(ET_Ignore, Param_Cell);
}

public void OnAllPluginsLoaded()
{
	CreateVsh1SuperJump();
	CreateVsh1Teleport();
	CreateWeighdown();
	CreateMaxspeed();
	CreateStun();
}

public void OnPluginEnd()
{
	DestroyMyDefinitions();
}

public void BFF_OnRoundStateChange(BffRoundState newState)
{
	Call_StartForward(roundStateChangeForward);
	Call_PushCell(newState);
	Call_Finish();
}

void AddRoundStateListener(Function func)
{
	AddToForward(roundStateChangeForward, INVALID_HANDLE, func);
}