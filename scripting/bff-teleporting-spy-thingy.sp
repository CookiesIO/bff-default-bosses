#pragma semicolon 1

#include <tf2items>

#pragma newdecls required

#include <sourcemod>
#include <tf2>
#include <tf2_stocks>
#include <sdkhooks>
#include <definitions>
#include <bossfightfortress-base>

#define PLUGIN_VERSION "1.0"

bool g_bIsCushionUsable[MAXPLAYERS+1];

public Plugin myinfo = 
{
	name = "Boss Fight Fortress - Teleporting Spy Thingy",
	author = "Cookies.io",
	description = "Sample boss for Boss Fight Fortress.",
	version = PLUGIN_VERSION,
	url = "http://forums.alliedmods.net"
};

public void OnPluginStart()
{
	CreateConVar("bossfightfortress_teleporting_spy_thingy_version", PLUGIN_VERSION, "Boss Fight Fortress - Teleporting Spy Thingy Version", FCVAR_NOTIFY|FCVAR_DONTRECORD|FCVAR_CHEAT);
}

public void OnAllPluginsLoaded()
{
	AddDefinitionCreatedListener(BFF_BOSS_GENERIC, BffGenericBossAvailable);
}

public void OnPluginEnd()
{
	DestroyMyDefinitions();
}

public void BffGenericBossAvailable(Definition definition)
{
	BffBossCreator creator = new BffBossCreator("Teleporting Spy Thingy", TFClass_Spy, GetMaxHealth, EquipBoss);
	creator.SetOnPlayerRunCmd(OnBossRunCmd);

	// This makes a new phase active at 75%, 50%, 25% and 10% health left, so the boss has 5 phases
	// Phase 0, which starts when the round starts
	// Phase 1, which starts when he reaches 75% health
	// Phase 2, which starts when he reaches 50% health
	// Phase 3, which starts when he reaches 25% health
	// Phase 4, which starts when he reaches 10% health
	float phases[] = { 0.75, 0.5, 0.25, 0.1 };
	creator.SetPhases(PhaseMode_PercentageHealth, PhaseReliability_AllowSkip, phases, sizeof(phases));

	// Set Blink and Cushion Blink abilities, they reduce their charge times as phases continue on
	creator.SetNormalAbility(AbilitySlot_Primary, 0, "Blink", OnChargeReleased_Blink, 0.5, 10.0);
	creator.SetChargeTimes(AbilitySlot_Primary, 1, 0.4, 7.5);
	creator.SetChargeTimes(AbilitySlot_Primary, 2, 0.3, 5.0);
	creator.SetChargeTimes(AbilitySlot_Primary, 3, 0.2, 3.0);
	creator.SetChargeTimes(AbilitySlot_Primary, 4, 0.1, 1.5);

	creator.SetContinuousAbility(AbilitySlot_Secondary, 0, "Cushion-Blink", OnChargeStart_Cushion, OnChargeReleased_Cushion, 0.2, 10.0);
	creator.SetAbilityFormatter(AbilitySlot_Secondary, 0, CushionBlinkFormatter);
	creator.SetChargeTimes(AbilitySlot_Secondary, 1, 0.2, 10.0);
	creator.SetChargeTimes(AbilitySlot_Secondary, 2, 0.2, 10.0);
	creator.SetChargeTimes(AbilitySlot_Secondary, 3, 0.2, 7.0);
	creator.SetChargeTimes(AbilitySlot_Secondary, 4, 0.4, 3.0);

	// Set ability Spah, which gives short bursts of invisibility
	creator.SetSpecialAbility(0, "Invis", SpecialChargeMode_Assisting, 500, 20.0, false, OnUseSpecial);
	creator.Finalize();
}

public int GetMaxHealth(float multiplier)
{
	return RoundToFloor(Pow(512.0 * multiplier, 1.1));
}

public void EquipBoss(BffClient client)
{
	client.RemoveAllWeapons();
	client.GiveWeapon("tf_weapon_sapper", 933, "426;0.5 ; 428;7.5", TFQuality_Strange);
	client.GiveWeapon("tf_weapon_knife", 574, "156;1 ; 160;1", TFQuality_Strange);
}

public Action OnBossRunCmd(BffClient client, int &buttons, int &impulse, float vel[3], float ang[3], int &weapon)
{
    float fallVelocity = client.GetEntPropFloat(Prop_Send, "m_flFallVelocity");
    if (g_bIsCushionUsable[client.Index])
    {
        if ((client.EntityFlags & FL_ONGROUND) == FL_ONGROUND || fallVelocity < 650.0)
        {
            g_bIsCushionUsable[client.Index] = false;
            client.UpdateHud();
        }
    }
    else
    {
        if ((client.EntityFlags & FL_ONGROUND) != FL_ONGROUND && fallVelocity > 650.0)
        {
            g_bIsCushionUsable[client.Index] = true;
            client.UpdateHud();
        }
    }
    
	if ((buttons & IN_ATTACK) == IN_ATTACK && client.IsInCondition(TFCond_Cloaked))
	{
		client.RemoveCondition(TFCond_Cloaked);
	}
	return Plugin_Continue;
}

// Special ability, Spah
public bool OnUseSpecial(BffClient client, float chargePercent)
{
	if (chargePercent == 1.0)
	{
		client.AddCondition(TFCond_Cloaked, 4.0);
		client.SetEntPropFloat(Prop_Send, "m_flInvisChangeCompleteTime", GetGameTime());
		return true;
	}
	return false;
}


// Inline charge ability, Blink
public bool OnChargeReleased_Blink(BffClient client, float cooldownPercent)
{
	if (cooldownPercent == 1.0)
	{
		client.Blink();
		return true;
	}
	return false;
}

// Inline charge ability, Cushion-Blink
public bool OnChargeStart_Cushion(BffClient client, float cooldownPercent)
{
	if (cooldownPercent == 1.0)
	{
		float fallVelocity = client.GetEntPropFloat(Prop_Send, "m_flFallVelocity");
		if (fallVelocity > 650.0)
		{
			client.AddCondition(TFCond_DeadRingered);
			client.SetEntPropFloat(Prop_Send, "m_flInvisChangeCompleteTime", GetGameTime());
			return true;
		}
	}
	return false;
}

public void OnChargeReleased_Cushion(BffClient client, float cooldownPercent)
{
	client.RemoveCondition(TFCond_Cloaked);
}

public void CushionBlinkFormatter(BffClient client, char[] buffer, int bufferSize, AbilityState abilityState, int percentage)
{
	if (abilityState == AbilityState_Ready)
	{
		if (g_bIsCushionUsable[client.Index])
		{
			Format(buffer, bufferSize, "Cushion-Blink usable!");
		}
		else
		{
			Format(buffer, bufferSize, "Cushion-Blink ready when you're falling");
		}
	}
	else
	{
		Format(buffer, bufferSize, "Cushion-Blink has %d charge", percentage);
	}
}